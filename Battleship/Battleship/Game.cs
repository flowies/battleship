﻿using System;
using System.Linq;
using Battleship.Board;

namespace Battleship
{
    class Game
    {
        private Player Player1 { get; set; }
        private Player Player2 { get; set; }

        public void Start()
        {
            Console.Write("Please enter player 1's name: ");
            var player1Name = Console.ReadLine();
            Player1 = new Player(player1Name);
            PlayerSetup(Player1);
            Console.WriteLine($"{Player1.Name} ships are all ready, change to player 2");
            Console.WriteLine("Please enter player 2's name: ");
            var player2Name = Console.ReadLine();
            Player2 = new Player(player2Name);
            PlayerSetup(Player2);
            Console.WriteLine($"{Player2.Name} ships are all ready, Lets Play");
            while (!Player1.HasLost && !Player2.HasLost)
            {
                BombFunction(Player1, Player2);
                if (!Player2.HasLost)
                {
                    BombFunction(Player2, Player1);
                }
            }
            Console.WriteLine(Player1.HasLost ? $"{Player2.Name} has won!" : $"{Player1.Name} has won!");
        }

        private void BombFunction(Player player, Player subject)
        {
            Console.WriteLine($"{player.Name} Please enter the bomb Coordinates");
            var bombcoordinates = Console.ReadLine();
            var xy = bombcoordinates?.Split(',');
            if (xy?.Length == 2)
            {
                // good array
                if (int.TryParse(xy[0], out var x) && int.TryParse(xy[1], out var y))
                {
                    // has ints
                    if (x <= Board.Board.BoardSize && y <= Board.Board.BoardSize)
                    {
                        // within board
                        var affectedPanels = subject.Board.Squares.Range(x, y, x, y).First();
                        if (affectedPanels.HasBeenFired)
                        {
                            Console.WriteLine($"{player.Name} you have already bomb here.");
                            return;
                        }
                        affectedPanels.HasBeenFired = true;
                        if (!affectedPanels.IsEmpty)
                        {
                            //hit!!!
                            Console.WriteLine($"{player.Name} you have hit a ship");
                            return;
                        }
                        Console.WriteLine($"{player.Name} you missed.");
                        return;
                    }

                }
                Console.WriteLine("Bad location please try again.");
                BombFunction(player, subject);
            }
        }

        private void PlayerSetup(Player player)
        {
            foreach (var placeShip in player.Ships)
            {
                var shipLocationOk = false;
                while (!shipLocationOk)
                {
                    Console.Write($"Please enter the location for {placeShip.Name} in format x,y,v or h - example 2,2,h: ");
                    var location = Console.ReadLine();
                    var xy = location?.Split(',');
                    if (xy?.Length == 3)
                    {
                        xy[2] = xy[2].ToLower();
                        // good array
                        if (int.TryParse(xy[0], out var x) && int.TryParse(xy[1], out var y) && (xy[2] == "v" || xy[2] == "h"))
                        {
                            // has ints
                            if (x <= Board.Board.BoardSize && y <= Board.Board.BoardSize)
                            {
                                // within board
                                if (xy[2] == "v")
                                {
                                    if (y + placeShip.Width <= Board.Board.BoardSize)
                                    {
                                        //good to go
                                        shipLocationOk = ShipLocationOk(player, x, y, x, y + placeShip.Width);
                                    }
                                }
                                else
                                {
                                    if (x + placeShip.Width <= Board.Board.BoardSize)
                                    {
                                        shipLocationOk = ShipLocationOk(player, x, y, x + placeShip.Width, y);
                                    }
                                }
                            }
                        }
                    }

                    if (!shipLocationOk)
                    {
                        Console.WriteLine("Bad location please try again.");
                    }
                }
            }
        }

        private bool ShipLocationOk(Player player, int x, int y, int endx, int endy)
        {
            var affectedPanels = player.Board.Squares.Range(x, y, endx, endy);
            if (!affectedPanels.All(k => k.IsEmpty))
            {
                return false;
            }

            foreach (var panel in affectedPanels)
            {
                panel.IsEmpty = false;
            }

            return true;

        }
    }
}
