﻿namespace Battleship
{
    public enum ShipType
    {
        Battleship,
        Destroyer,
        Cruiser,
        Submarine,
        Carrier
    }
}