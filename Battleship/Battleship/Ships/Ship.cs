﻿namespace Battleship
{
    public class Ship
    {
        public Ship(ShipType type)
        {
            Name = type.ToString();
            switch (type)
            {
                case ShipType.Battleship:
                    Width = 4;
                    break;
                case ShipType.Destroyer:
                    Width = 2;
                    break;
                case ShipType.Cruiser:
                    Width = 3;
                    break;
                case ShipType.Submarine:
                    Width = 3;
                    break;
                case ShipType.Carrier:
                    Width = 5;
                    break;
            }

            Type = type;
        }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Hits { get; set; }
        public ShipType Type { get; set; }
        public bool IsSunk => Hits >= Width;
    }
}
