﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship
{
    public class Player
    {
        public string Name { get; set; }
        public Board.Board Board { get; set; }
        public List<Ship> Ships { get; set; }
        public bool HasLost => Ships.All(x => x.IsSunk);

        public Player(string name)
        {
            Name = name;
            Ships = new List<Ship>();
            foreach (var i in (ShipType[])Enum.GetValues(typeof(ShipType)))
            {
                Ships.Add(new Ship(i));
            }
            Board = new Board.Board();
        }
    }
}
