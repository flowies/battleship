﻿using System.Collections.Generic;
using System.Linq;

namespace Battleship.Board
{
    public static class SquareExtensions
    {
        public static List<Square> Range(this List<Square> panels, int startRow, int startColumn, int endRow, int endColumn)
        {
            return panels.Where(x => x.Coordinates.Row >= startRow
                                     && x.Coordinates.Column >= startColumn
                                     && x.Coordinates.Row <= endRow
                                     && x.Coordinates.Column <= endColumn).ToList();
        }
    }
}
