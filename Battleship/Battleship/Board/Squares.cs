﻿using Battleship.Board;

namespace Battleship
{
    public class Square
    {
        public Square(int row, int column)
        {
            Coordinates = new Coordinates(row, column);
        }
        public Coordinates Coordinates { get; private set; }
        public bool IsEmpty { get; set; } = true;
        public bool HasBeenFired { get; set; }
    }
}
