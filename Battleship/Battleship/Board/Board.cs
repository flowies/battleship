﻿using System.Collections.Generic;

namespace Battleship.Board
{
    public class Board
    {
        public const int BoardSize = 10;

        public List<Square> Squares { get; set; }

        public Board()
        {
            Squares = new List<Square>();
            for (var i = 1; i <= BoardSize; i++)
            {
                for (var j = 1; j <= BoardSize; j++)
                {
                    Squares.Add(new Square(i, j));
                }
            }
        }
    }
}
