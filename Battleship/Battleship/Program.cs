﻿using System;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            var playAgain = true;
            while (playAgain)
            {
                var currentGame = new Game();
                currentGame.Start();
                Console.Write("Play again (y/n)");
                var again = Console.ReadLine();
                if (again == null || again.ToLower() != "y")
                {
                    playAgain = false;
                }
            }

        }
    }
}
